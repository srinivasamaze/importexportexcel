﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UploadExcelData
{
    public partial class ExcelToSQL : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public static string path = @"C:\Users\tech_\Desktop\ExcelToSql\UploadExcelData\UploadExcelData\UploadExcelData\ExcelFolder\Book1.xlsx";
        public static string connStr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source = "+ path + "; Extended Properties = Excel 12.0";
        DataSet ds;
        protected void Button1_Click(object sender, EventArgs e)
        {
            OleDbConnection OleDbCon = new OleDbConnection(connStr);
            OleDbCommand cmd = new OleDbCommand("SELECT Itemid,ItemType,ItemName,Quantity,Unit FROM [Sheet1$]", OleDbCon);
            OleDbDataAdapter objAdapter1 = new OleDbDataAdapter(cmd);
            ds = new DataSet();
            objAdapter1.Fill(ds);
            OleDbCon.Open();
            OleDbDataReader dr = cmd.ExecuteReader();
            string conStr = "Data Source=localhost;Initial Catalog=ExcelData0;User ID=sa;Password=Smartdocs@123";
            //Bulk copy to SQL
            SqlBulkCopy bulkInsert = new SqlBulkCopy(conStr);
            bulkInsert.DestinationTableName = "ItemInfo";
            bulkInsert.WriteToServer(dr);
            OleDbCon.Close();
        }
    }
}